#include <algorithm>
#include <iostream>
#include <string>
#include <vector>

static const int kSigma = 256;

class SuffixArr {
 public:
  std::string str;
  int len;
  std::vector<int> classes;  // классы эквивалентности суффиксов
  std::vector<int>
      permutation;  // индексы начал суффиксов в порядке возрастания
  SuffixArr(std::string& inp_st) { Build(inp_st); }

 private:
  void Build(std::string& inp_st) {
    str = inp_st;
    len = str.size();
    permutation.resize(len);
    classes.resize(len);
    std::vector<int> cnt(
        len, 0);  // счётчик эквивалентных элементов для сортировки подсчётом
    std::vector<int> cnt_char(kSigma, 0);
    // step 0
    for (int i = 0; i < len; ++i) {
      ++cnt_char[str[i]];
    }
    for (int i = 1; i < kSigma; ++i) {
      cnt_char[i] += cnt_char[i - 1];
    }
    for (int i = len - 1; i >= 0; --i) {
      permutation[--cnt_char[str[i]]] = i;
    }
    classes[permutation[0]] = 0;
    for (int i = 1; i < len; ++i) {
      classes[permutation[i]] = classes[permutation[i - 1]];
      if (str[permutation[i]] != str[permutation[i - 1]]) {
        ++classes[permutation[i]];
      }
    }

    int cur_len = 1;
    std::vector<int> permut_new(len);
    std::vector<int> classes_new(len);
    while (cur_len < len) {
      cur_len *= 2;
      for (int i = 0; i < len; ++i) {
        permut_new[i] = permutation[i] - (cur_len / 2);
        if (permut_new[i] < 0) {
          permut_new[i] += len;
        }
      }
      std::fill(cnt.begin(), cnt.end(), 0);
      for (int i = 0; i < len; ++i) {
        ++cnt[classes[i]];
      }
      for (int i = 1; i < len; ++i) {
        cnt[i] += cnt[i - 1];
      }
      for (int i = len - 1; i >= 0; --i) {
        permutation[--cnt[classes[permut_new[i]]]] = permut_new[i];
      }
      classes_new[permutation[0]] = 0;
      for (int i = 1; i < len; ++i) {
        classes_new[permutation[i]] = classes_new[permutation[i - 1]];
        if (classes[permutation[i]] != classes[permutation[i - 1]]) {
          ++classes_new[permutation[i]];
        } else if (classes[(permutation[i] + cur_len / 2) % len] !=
                   classes[(permutation[i - 1] + cur_len / 2) % len]) {
          ++classes_new[permutation[i]];
        }
      }
      classes = classes_new;
    }
  }
};

int main() {
  std::ios_base::sync_with_stdio(false);
  std::cin.tie(nullptr);

  std::string str;
  std::cin >> str;

  SuffixArr sa(str);
  for (int i = 0; i < static_cast<int>(str.size()); ++i) {
    std::cout << str[(sa.permutation[i] + str.size() - 1) % str.size()];
  }
}
