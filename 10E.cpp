#include <string.h>

#include <algorithm>
#include <array>
#include <cstring>
#include <iostream>
#include <optional>
#include <queue>
#include <string>
#include <vector>

const int kSigma = 10;
const int kShift = 48;

struct Node {
  std::array<std::optional<int>, kSigma> to;
  int depth;
  int term_in_subtree;
  bool used;
  Node() {
    depth = 0;
    term_in_subtree = 0;
    used = false;
  }
};

class Trie {
 private:
 public:
  std::vector<Node> tr;
  static int GetShiftedInd(char letter) { return letter - kShift; }
  void Add(std::string str) {
    if (tr.empty()) {
      tr.push_back(Node());
    }
    int cur_node = 0;
    int cur_depth = 1;
    for (int i = 0; i < static_cast<int>(str.size()); ++i) {
      if (!tr[cur_node].to[GetShiftedInd(str[i])].has_value()) {
        tr[cur_node].to[GetShiftedInd(str[i])] = std::make_optional(tr.size());
        tr.push_back(Node());
      }
      cur_node = tr[cur_node].to[GetShiftedInd(str[i])].value();
      tr[cur_node].depth = cur_depth;
      ++cur_depth;
      tr[cur_node].term_in_subtree += 1;
      if (!tr[cur_node]
               .to[GetShiftedInd(str[str.size() - 1 - i])]
               .has_value()) {
        tr[cur_node].to[GetShiftedInd(str[str.size() - 1 - i])] =
            std::make_optional(tr.size());
        tr.push_back(Node());
      }
      cur_node =
          tr[cur_node].to[GetShiftedInd(str[str.size() - 1 - i])].value();
      tr[cur_node].depth = cur_depth;
      ++cur_depth;
      tr[cur_node].term_in_subtree += 1;
    }
  }
  void Dfs(int cur_vert, int max_suspicious_size, std::vector<int>& answers) {
    if ((cur_vert != 0) && (tr[cur_vert].depth % 2 == 0) &&
        (tr[cur_vert].term_in_subtree >= max_suspicious_size)) {
      answers[tr[cur_vert].depth / 2] += 1;
    }
    for (int i = 0; i < kSigma; ++i) {
      if ((tr[cur_vert].to[i].has_value()) &&
          (!tr[tr[cur_vert].to[i].value()].used)) {
        Dfs(tr[cur_vert].to[i].value(), max_suspicious_size, answers);
      }
    }
    tr[cur_vert].used = true;
  }
};

int main() {
  std::ios_base::sync_with_stdio(false);
  std::cin.tie(nullptr);

  int num;
  int max_suspicious_size;
  std::cin >> num >> max_suspicious_size;
  Trie my_trie;
  int maxi_size = 0;
  for (int i = 0; i < num; ++i) {
    std::string str;
    std::cin >> str;
    my_trie.Add(str);
    maxi_size = std::max(maxi_size, static_cast<int>(str.size()));
  }
  int number_of_queries;
  std::cin >> number_of_queries;
  std::vector<int> answers(maxi_size + 1, 0);
  my_trie.Dfs(0, max_suspicious_size, answers);
  for (int i = 0; i < number_of_queries; ++i) {
    int size_of_same_group;
    std::cin >> size_of_same_group;
    if (size_of_same_group == 0) {
      std::cout << static_cast<int>(max_suspicious_size <= num) << '\n';
    } else {
      std::cout << answers[size_of_same_group] << '\n';
    }
  }
}
