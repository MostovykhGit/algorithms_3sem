#include <assert.h>

#include <algorithm>
#include <array>
#include <cstring>
#include <iostream>
#include <optional>
#include <queue>
#include <string>
#include <vector>

const int kSigma = 26;

struct Node {
  std::array<std::optional<int>, kSigma> to;
  bool term;
  std::vector<int> entries;
  Node() { term = false; }
};

class AhoKorasik {
 private:
 public:
  std::vector<Node> trie;
  std::vector<int> link;
  std::vector<int> compressed_link;
  std::vector<std::vector<int>> go;
  static int GetShiftedInd(char letter) { return letter - 'a'; }
  void Add(std::string& str) {
    if (trie.empty()) {
      trie.push_back(Node());
    }
    int current_node = 0;
    for (int i = 0; i < static_cast<int>(str.size()); ++i) {
      if (!trie[current_node].to[GetShiftedInd(str[i])].has_value()) {
        trie[current_node].to[GetShiftedInd(str[i])] =
            std::make_optional(trie.size());
        trie.push_back(Node());
      }
      current_node = trie[current_node].to[GetShiftedInd(str[i])].value();
    }
    trie[current_node].term = true;
  }
  void Build() {
    link.resize(trie.size());
    compressed_link.resize(trie.size());
    go.resize(trie.size());
    std::fill(go.begin(), go.end(), std::vector<int>(kSigma, 0));
    link[0] = 0;
    compressed_link[0] = 0;
    for (int letter = 0; letter < kSigma; ++letter) {
      if (trie[0].to[letter].has_value()) {
        go[0][letter] = trie[0].to[letter].value();
      } else {
        go[0][letter] = 0;
      }
    }
    std::queue<int> vertex_queue;
    vertex_queue.push(0);
    while (!vertex_queue.empty()) {
      int current_vertex = vertex_queue.front();
      vertex_queue.pop();
      for (int letter = 0; letter < kSigma; ++letter) {
        std::optional<int> next_vertex = trie[current_vertex].to[letter];
        if (!next_vertex.has_value()) {
          continue;
        }
        link[next_vertex.value()] =
            (current_vertex == 0 ? 0 : go[link[current_vertex]][letter]);
        if (trie[link[next_vertex.value()]].term) {
          compressed_link[next_vertex.value()] = link[next_vertex.value()];
        } else {
          compressed_link[next_vertex.value()] =
              compressed_link[link[next_vertex.value()]];
        }
        for (int dr = 0; dr < kSigma; ++dr) {
          if (trie[next_vertex.value()].to[dr].has_value()) {
            go[next_vertex.value()][dr] =
                trie[next_vertex.value()].to[dr].value();
          } else {
            go[next_vertex.value()][dr] = go[link[next_vertex.value()]][dr];
          }
        }
        vertex_queue.push(next_vertex.value());
      }
    }
  }
  void Solution(const std::string& text, std::vector<std::string>& strs) {
    std::vector<int> max_suf_in_trie(text.size(), 0);
    max_suf_in_trie[0] = (trie[0].to[GetShiftedInd(text[0])].has_value()
                              ? trie[0].to[GetShiftedInd(text[0])].value()
                              : 0);
    if (max_suf_in_trie[0] != 0) {
      trie[max_suf_in_trie[0]].entries.push_back(0);
    }
    for (int i = 1; i < static_cast<int>(text.size()); ++i) {
      max_suf_in_trie[i] = go[max_suf_in_trie[i - 1]][GetShiftedInd(text[i])];
      int compress = max_suf_in_trie[i];
      while (compress != 0) {
        trie[compress].entries.push_back(i);
        compress = compressed_link[compress];
      }
    }
    for (int i = 0; i < static_cast<int>(strs.size()); ++i) {
      int current_node = 0;
      for (int j = 0; j < static_cast<int>(strs[i].size()); ++j) {
        assert(trie[current_node].to[GetShiftedInd(strs[i][j])].has_value());
        current_node = trie[current_node].to[GetShiftedInd(strs[i][j])].value();
      }
      std::cout << trie[current_node].entries.size() << ' ';
      for (int k = 0; k < static_cast<int>(trie[current_node].entries.size());
           ++k) {
        std::cout << trie[current_node].entries[k] - strs[i].size() + 2 << ' ';
      }
      std::cout << '\n';
    }
  }
};

int main() {
  std::ios_base::sync_with_stdio(false);
  std::cin.tie(nullptr);

  std::string text;
  std::cin >> text;
  int num;
  std::cin >> num;
  AhoKorasik ak;
  std::vector<std::string> strs(num);
  for (int i = 0; i < num; ++i) {
    std::cin >> strs[i];
    ak.Add(strs[i]);
  }
  ak.Build();
  ak.Solution(text, strs);
}
